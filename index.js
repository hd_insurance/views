import FLInput from "./flinput";
import FLDate from "./fldate";
import FLTextArea from "./fltextarea";
import FLTime from "./fltime";
import FLSelect from "./flselect"; 
import FLSelectMultiple from "./flselectmultiple";
import FLAddress from "./fladdress";
import FLAmount from "./flamount";
import FLBankList from "./flbanklist";
import FLAirport from "./flairport";
import FLSuggesstion from "./flsuggestion/index.jsx";
import FLMultiSelect from "./flmultiselect"; // relation
import FLRadio from "./flradio"
import FLCheckBox from "./flcheckbox"
import FLSwitch from "./flswitch"
import FLYear from "./flyear"
import ListItem from "./listitem"
import Tabs from "./tabs"
import TabsItem from "./tabs/item.js"
import DivRender from "./div"
import UploadFrame from "./uploadframe"
import SingleUpload from "./uploadframe/single-upload.js"
import Text from "./text"
import BeneficiaryBank from "./beneficiary_bank"
import BeneficiaryUser from "./beneficiary_user"
import OTPConfirm from "./otp_confirm"
import WrapSelect from "./common/wrapselect";
import LoadingComponent from "./loadingComponent";


export  {
  FLInput,
  FLDate,
  FLSelect,
  FLTextArea,
  FLTime,
  FLSelectMultiple,
  FLAddress,
  FLAmount,
  FLBankList,
  FLAirport,
  FLMultiSelect,
  FLRadio,
  FLCheckBox,
  FLSwitch,
  FLYear,
  ListItem,
  Tabs,
  TabsItem,
  DivRender,
  UploadFrame,
  Text,
  FLSuggesstion,
  BeneficiaryBank,
  BeneficiaryUser,
  OTPConfirm,
  SingleUpload,
  WrapSelect,
  LoadingComponent
};

