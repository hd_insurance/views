import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
} from "react";

const TextRender = forwardRef((props, ref) => {
  const [text, setText] = useState(props.value);
  useEffect(() => {
    setText(props.value)
  }, [props.value]);

  return (
    <span className={props.extend_class}>{text}</span>
  );
});

export default TextRender;
