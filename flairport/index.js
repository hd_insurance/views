import React, { useEffect, useState } from 'react';
import Popover from 'react-popover';
import PopSelect from './pop-airport.js';
import FLInput from '../flinput';
import {isServer} from "../platform.js";

const AirportInput = (props) => {
  const [openAddress, setOpenAddress] = useState(false);
  const [addressValue, setAddressValue] = useState(props.value.label);

  const [openList, setOpenList] = useState(false);

  const [readonly, setReadonly] = useState(false);

  if (isServer) return null;

  const locationFormSelect = (vl) => {
    setAddressValue(vl.label);
    props.changeEvent(vl);
    setTimeout(() => {
      setOpenAddress(false);
    }, 200);
  };

  const popoverPropsAddress = {
    isOpen: openAddress,
    place: 'below',
    preferPlace: 'right',
    onOuterAction: () => setOpenAddress(false),
    body: [<PopSelect addressSelect={locationFormSelect} />],
  };

  const blurInput = () => {
    setReadonly(false);
  };
  const focusInput = () => {
    setOpenAddress(true);
    setReadonly(true);
  };

  return (
    <Popover {...popoverPropsAddress}>
      <FLInput
        disable={props.disable}
        loading={props.loading}
        label={props.label}
        onFocus={(e) => focusInput()}
        readonly={readonly}
        value={addressValue}
        required={props.required != undefined ? props.required : true}
        dropdown={true}
        hideborder={props.hideborder}
        position={props.position}
        onBlur={() => blurInput()}
      />
    </Popover>
  );
};
export default AirportInput;
