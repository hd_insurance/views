import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useRef,
  useImperativeHandle,
} from "react";

import { Accordion } from 'react-bootstrap';
import UploadItem from './UploadItem.jsx';
import FilePreviewer, {FilePreviewerThumbnail} from './preview';
import { isServer } from "../platform.js";

const dataxxx = [

  ]
const default_lang = {
  "select_file": "Chọn file upload",
  "no_file": "Không có file"
}

const UploadRender = forwardRef((props, ref) => {
  if (isServer) return null;
  const divref = useRef(null)
  const [language, setLanguage] = useState(props.language?props.language:default_lang);
  const [current, setCurrent] = useState("0");
  const [data, setData] = useState(props.data || dataxxx);
  const [viewOnly, setViewOnly] = useState(props.viewOnly);
  const [fileData, setFileData] = useState([]);

  const [height, setHeight] = useState(90)


  useEffect(() => {
    if(props.data){
      setData(props.data)
    }
  },[props.data]);
  useEffect(() => {
    setViewOnly(props.viewOnly)
  },[props.viewOnly]);

   useEffect(() => {
    if(divref.current){
      if(divref.current.clientWidth>0){
        setHeight(divref.current.clientWidth/4)
      }
    }
    
   },[divref.current])


  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };


 const onFileUpload = (file_key, data)=>{
    if(file_key && data){
      fileData.push({
          "FILE_KEY": data.key,
          "FILE_NAME": `${data.key}_${file_key}`,
          "FILE_ID": file_key
        })
      setFileData(fileData)
      if(props.onFileUpdate){
        props.onFileUpdate(fileData)
      }
    }else{
      props.onFileUpdate(false)
    }
 }




 const onFileDelete = (fId, new_arr)=>{
    const newArray = fileData.filter(function( obj ) {
        return obj.FILE_ID !== fId;
    });
    setFileData(newArray)
    if(props.onFileUpdate){
      props.onFileUpdate(newArray)
    }
 }

 const FileItem = ({files, index})=>{
  if(files.list[index]){
      return (<div className="col-md-file box box1">
               <div class="box box1">
                <FilePreviewerThumbnail file={{url: files.list[0].source}}/>
               </div> 
              </div>)
  }
  return (<div className="col-md-file box box1">
           <div class="box box1">
           
           </div> 
          </div>)
 }


  return (
    <div className={`upload-frame-container ${props.extend_class}`}>
     
       <Accordion defaultActiveKey={"0"} activeKey={current}>

       {data.map((group_frame, index)=>{

          return (<div key={index} className={`${index.toString() == current?"activated":"unactivated"} upload-f-item`}>
                    <div className="upload-header" onClick={(e)=>{
                      setCurrent(index.toString())
                    }}>
                     <div className="title">{group_frame.group_name}</div>
                     <i class="fas fa-angle-down"></i>
                    </div>
                    <Accordion.Collapse className="upload-body" eventKey={index.toString()}>
                      <div className="upload-ctx">

                      <div className="row row-flex">

                        {group_frame.files.map((file, index)=>{
                          {/*console.log("group_frame ", file.list)*/}
                          return (
                            
                            <div className={`${props.col_layout==2?"col-md-6":"col-md-4"} pdt-15`} ref={divref}>
                              <div className="file-item" style={{paddingBottom: height+5}}>
                                <div className="up-label">{file.label}</div>
                                <div className="file-upcont">
                                  <UploadItem 
                                    viewOnly={viewOnly}
                                    data={{...group_frame, ...file}}
                                    language={language}
                                    onFileUpload={onFileUpload}
                                    onFileDelete={onFileDelete}
                                    listPreview={file.list}
                                    />
                                </div>


                              </div>
                            </div>


                          )
                        })}
                      </div>



                      </div>
                    </Accordion.Collapse>
                  </div>)
       })}
        
        
      </Accordion>


    </div>
  );
});

export default UploadRender;
