import React, { useEffect, useCallback, useState, useRef} from "react";
import Dropzone, {useDropzone} from 'react-dropzone'
import api from '../services/Network';
import FilePreviewer, {FilePreviewerThumbnail} from './preview';
import UploadPreview from "./dialogpreview"

const default_lang = {
	"select_file": "Chọn file upload",
	"no_file": "Không có file"
}
const UploadItem = (props) => {
   const ref = useRef(null)
   const [height, setHeight] = useState(370)
   const [width, setWidth] = useState(370)
   const [files, setFiles] = useState(props.listPreview||[])
   const [isPreview, setPreview] = useState(props.viewOnly||false)
   const [loading, setLoading] = useState(false)
   const [openPreview, setOpenPreview] = useState(false)
   const [language, setLanguage] = useState(props.language?props.language:default_lang);



   useEffect(() => {
   	if(ref.current.clientWidth>0){
   		setHeight(ref.current.clientHeight)
    	setWidth(ref.current.clientWidth)
   	}
   })
   useEffect(() => {
   	setPreview(props.viewOnly)
   }, [props.viewOnly])


   useEffect(() => {
   	setLanguage(props.language)
   }, [props.language])


   const onFileUpload = async (out_file)=>{
   	try{
   		setLoading(true)
   		let formData = new FormData();
   		formData.append("files", out_file[0]);
	      const response =  await api.upload(formData)
	      if(response.data[0].file_key){
	      	   if(props.onFileUpload){
			      	props.onFileUpload(response.data[0].file_key, props.data)
			      }
			      const newf = files
			      const fileReader = new FileReader();
			      fileReader.onload = fileLoad => {
			          const { result } = fileReader;
			          newf.unshift({
			            file: out_file[0],
			            fileId: response.data[0].file_key,
			            url: result
			          })
			          setFiles([...newf])
			      };
			      fileReader.readAsDataURL(out_file[0]);
	      }else{

	      }
	      setLoading(false)
   	}catch(e){
   		setLoading(false)
   		if(props.onFileUpload){
   			props.onFileUpload(false, false)
   		}
   	}
      

      
      // props.onFileUpload(config, response.data[0].file_key)

     
  }
  
  const onImageClick = ()=>{
  		setOpenPreview(true)
  }

  const removeFile = (remove_index)=>{
  	const fId = files[remove_index].fileId

  	
	const newarr = [ ...files ]
	newarr.splice(remove_index, 1);
	setFiles(newarr)
	if(props.onFileDelete){
  		props.onFileDelete(fId, newarr)
  	}
	setOpenPreview(false)
  }


   return(<div className="upload-item" ref={ref}>

   	{openPreview&&<UploadPreview files={files} index={0} removeFile={removeFile} isPreview={isPreview} onClose={()=>setOpenPreview(false)}/>}

   	{loading&&<div className="dot-load upload-loading">
   		<div className="dots dot8">
   			<div className="dot"/>
   			<div className="dot"/>
   			<div className="dot"/>
   		</div>

   	</div>}




   	{(files.length > 0)?<div className="list-fmk2">

   		{!isPreview&& <div style={{width: width/4, height: width/4 }}>
   			<div className="item-upload-mk2">
   				<Dropzone
				    accept={'image/jpeg, image/png, application/pdf'}
				    onDrop={f => {
				    	onFileUpload(f)
				    }}>
					  {({getRootProps, getInputProps}) => (
					    <div className="item-container">
					      <div
					        {...getRootProps({
					          className: 'dropzone',
					          onDrop: event => event.stopPropagation()
					        })}
					      >
					        <input {...getInputProps()} />
					        <div className="label">
					         <i class="fas fa-cloud-upload-alt"></i>
					        </div>
					      </div>
					    </div>
					  )}
					</Dropzone>


   			</div>
   		</div>}
   		{files.slice(0, 3).map((fitem, index)=>{
   			
   			if(isPreview){
   				return <div key={index} style={{width: width/4, height: width/4 }} onClick={()=>onImageClick()}>
   					<div className="item-upload-mk2" >
   						<FilePreviewerThumbnail ispdf={fitem.pdf} file={{url: fitem.url}}/>
   					</div>
   				</div>
   			}else{
   				return(<div key={index} style={{width: width/4, height: width/4 }} onClick={()=>onImageClick()}>
		   				<div className="item-upload-mk2" >
		   					{(index==2 && files.length >3)&&<div className="num-item-upload">
		   						<div><span>+{files.length - 3}</span></div>
		   					</div>}
		   					<FilePreviewerThumbnail ispdf={fitem.pdf} file={{url: fitem.url}}/>
		   				</div>
		   			</div>)
   			}
   			
   		})
   		
   		}



   	</div>:<div>
   		<Dropzone
		    accept={'image/jpeg, image/png, application/pdf'}
		    disabled={isPreview}
		    onDrop={f => {
		    	onFileUpload(f)
		    }}>
			  {({getRootProps, getInputProps}) => (
			    <div className="item-container" style={{height: width/4 }}>
			      <div
			        {...getRootProps({
			          className: 'dropzone',
			          onDrop: event => event.stopPropagation()
			        })}
			      >
			        <input {...getInputProps()} />
			        {props.viewOnly?<div className="label">


			         <span>{language.no_file}</span>
			        </div>:<div className="label">
			         <i class="fas fa-cloud-upload-alt"></i>
			         <span>{language.select_file}</span>
			        </div>}
			      </div>
			    </div>
			  )}
			</Dropzone>

   	</div>}
	    



    </div>
);
}

export default UploadItem;
