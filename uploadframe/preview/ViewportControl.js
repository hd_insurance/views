import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const ViewportControl = props => {
  if (props.hidden) {
    return null;
  }

  return (
    <div className="preview-icons">
      <Button onClick={props.onZoomIn}>
        +
      </Button>

      <Button onClick={props.onZoomOut} className="preview-icons">
        -
      </Button>

      <Button onClick={props.onFitToScreen} className="preview-icons">
        >
      </Button>
    </div>
  );
};

ViewportControl.propTypes = {
  onZoomIn: PropTypes.func.isRequired,
  onZoomOut: PropTypes.func.isRequired,
  onFitToScreen: PropTypes.func.isRequired,
  hidden: PropTypes.bool,
};

export default ViewportControl;
