import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";

import { Accordion } from 'react-bootstrap';
import UploadItem from './UploadItem.jsx';
import FilePreviewer, {FilePreviewerThumbnail} from './preview';
import { isServer } from "../platform.js";

const dataxxx = [

  ]
const default_lang = {
  "select_file": "Chọn file upload",
  "no_file": "Không có file"
}

const UploadRender = forwardRef((props, ref) => {
  if (isServer) return null;
  const [current, setCurrent] = useState("0");
  const [data, setData] = useState(props.data || dataxxx);
  const [viewOnly, setViewOnly] = useState(props.viewOnly);
  const [fileData, setFileData] = useState([]);
  const [language, setLanguage] = useState(props.language?props.language:default_lang);

  useEffect(() => {
    if(props.data){
      setData(props.data)
    }
  },[props.data]);
  useEffect(() => {
    setViewOnly(props.viewOnly)
  },[props.viewOnly]);


  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };


 const onFileUpload = (file_key, data)=>{
    // console.log("file key ", file_key, data)
    fileData.push({
        "FILE_KEY": data.key,
        "FILE_NAME": `${data.key}_${file_key}`,
        "FILE_ID": file_key
      })
    setFileData(fileData)
    if(props.onFileUpdate){
      props.onFileUpdate(fileData)
    }
 }




 const onFileDelete = (fId, new_arr)=>{
    const newArray = fileData.filter(function( obj ) {
        return obj.FILE_ID !== fId;
    });
    setFileData(newArray)
    if(props.onFileUpdate){
      props.onFileUpdate(newArray)
    }
 }

  return (
    <div className={`upload-frame-container ${props.extend_class}`}>
          <div className="upload-frame-container">
          <div className="upload-f-item">
          <div className="upload-body">
          <div className="upload-ctx2">
          <div className="file-item">
            <UploadItem viewOnly={false} data={{key: "BENEFICIARY_DOCUMENT"}} language={language} onFileUpload={onFileUpload} onFileDelete={onFileDelete} />
            </div>
            </div>
            </div>
          </div>
          </div>


    </div>
  );
});

export default UploadRender;
