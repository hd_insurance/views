//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import Skeleton from 'react-loading-skeleton';
import styles from '../style.module.css';


const Main = forwardRef((props, ref) => {
 const [items, setItems] = useState([]);
 useEffect(() => {
    setItems(props.product_items)
  }, [props.product_items]);
  return (
    <div id={styles.float_label}>
      {props.loading ? <Skeleton width={'100%'} /> : null}

      {items.map((item, index)=>{
          const Component = item.item_component
          return (<Component {...item.props}/>)
      })}
      
    </div>
  );
});

export default Main;
