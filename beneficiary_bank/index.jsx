import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import FLInput from "../flinput";
import FLSelect from "../flselect";
import FLSwitch from "../flswitch";
import FLBankList from "../flbanklist";


const default_language = {
  "transfer": "Hình thức chuyển khoản",
  "bank_type": "Loại ngân hàng",
  "bank_name": "Tên ngân hàng",
  "bank_branch": "Chi nhánh",
  "bank_number": "Số tài khoản",
  "bank_account": "Tên chủ tài khoản",
  "bene_bank_name": "Tên ngân hàng thụ hưởng",
  "swiffcode": "Swift code",
  "address_bank": "Địa chỉ ngân hàng thụ hưởng",
  "address_acount": "Địa chỉ người thụ hưởng",
  "pay_via_middle_bank": "Chuyển tiền qua ngân hàng trung gian",
  "middle_bank_name": "Tên ngân hàng trung gian",
  "bank_local":"Ngân hàng tại Việt Nam",
  "bank_foreign": "Ngân hàng ở nước ngoài"
}

const transfer_type_data = [
    {
      label: "Qua số tài khoản",
      value: "STK"
    }
  ]

const bank_name_data = [
    {
      label: "HDBank",
      value: "ND"
    },
    {
      label: "ACB",
      value: "QT"
    }
  ]

const bank_branch_data = [
    {
      label: "Chi nhánh Hoàn Kiếm",
      value: "ND"
    }
  ]


const BankRender = forwardRef((props, ref) => {
  const [language, setLanguage] = useState(props.language?props.language:default_language);
  const [transfer_type, setTransferType] = useState("STK");
  const [transfer_type_label, setTransferTypeLabel] = useState(null);
  const [bank_type, setBankType] = useState("ND");
  const [bank_type_label, setBankTypeLabel] = useState(null);

  const [bank_code, setBankCode] = useState("");
  const [bank_name, setBankName] = useState({label: '', value: ''});
  const [bank_branch, setBranch] = useState("");
  const [bank_number, setBankNumber] = useState("");
  const [bank_account, setBankAccount] = useState("")

  const [bank_name_qt, setBankNameQT] = useState("");
  const [bank_swiffcode, setBankSwiffcode] = useState("");
  const [bank_address, setBankAddress] = useState("");
  const [account_address, setAccountAddress] = useState("");
  const [isMiddle, setMiddle] = useState(false);
  const [middle_bank_name, setMiddleBankName] = useState("");
  const [middle_swiff, setMiddleSwiff] = useState("");

  const [disableBranch, setDisableBranch] = useState(true);

  const bank_type_data = [
    {
      label: language.bank_local,
      value: "ND"
    },
    {
      label: language.bank_foreign,
      value: "QT"
    }
  ]

  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };

  useImperativeHandle(ref, () => ({ 
      getData() {
        if(bank_type == "ND"){
          return {
            transfer_type: transfer_type,
            pay_method_label: transfer_type_label?transfer_type_label:transfer_type_data[0].label,
            bank_type_label: bank_type_label,
            bank_type: bank_type,
            bank_code: bank_code,
            bank_name: bank_name.label,
            bank_branch: bank_branch,
            bank_number: bank_number,
            isMiddle: false,
            bank_account: bank_account.toUpperCase()
          }
        }else if(bank_type == "QT"){
          return {
            transfer_type: transfer_type,
            pay_method_label: transfer_type_label?transfer_type_label:transfer_type_data[0].label,
            bank_type_label: bank_type_label,
            bank_type: bank_type,
            bank_name: bank_name_qt,
            bank_swiffcode: bank_swiffcode,
            bank_address: bank_address,
            bank_account: bank_account.toUpperCase(),
            bank_number: bank_number,
            account_address: account_address,
            isMiddle: isMiddle,
            middle_bank_name: isMiddle?middle_bank_name:"",
            middle_swiff: isMiddle?middle_swiff:""
          }
        }
      }

    }))
  //changeEvent

  return (
    <div className={`${props.extend_class}`} onClick={handleOnclick}>
        <div className="row">
          <div className="col-md-4  mt-15">
            <FLSelect
                  data={transfer_type_data}
                  value={transfer_type}
                  label={language.transfer}
                  required={true}
                  changeEvent={(value)=>{
                    const myArray = transfer_type_data.filter(function( obj ) {
                        return obj.value == value;
                    });
                    console.log(myArray[0].label)
                    setTransferTypeLabel(myArray[0].label)
                    setTransferType(value)
                  }}
                />
          </div>

          {transfer_type=="STK"&&<div className="col-md-4 mt-15">
            <FLSelect
                  data={bank_type_data}
                  label={language.bank_type}
                  required={true}
                  value={bank_type}
                  changeEvent={(value)=>{
                    const myArray = bank_type_data.filter(function( obj ) {
                        return obj.value == value;
                    });

                    setBankTypeLabel(myArray[0].label)
                    setBankType(value)
                  }}
                />
          </div>}

           {(transfer_type=="STK" && bank_type == "ND")&&<div className="col-md-4  mt-15">
            <FLBankList
                  data={bank_branch_data}
                  label={language.bank_name}
                  value={bank_name}
                  changeEvent={(value)=>{
                    setDisableBranch(false)
                    setBankName(value)
                  }}
                  required={true}
                />

          </div>}



        </div>
        {(transfer_type=="STK" && bank_type == "ND")&&<div className="row bank_noidia">
          <div className="col-md-4 mt-15">
              <FLInput
                  label={language.bank_branch}
                  required={true}
                  disable={disableBranch}
                  value={bank_branch}
                  changeEvent={(value)=>{
                    setBranch(value)
                  }}
                />
          </div>
          <div className="col-md-4 mt-15">
              <FLInput
                  label={language.bank_number}
                  value={bank_number}
                  changeEvent={(value)=>{
                    setBankNumber(value)
                  }}
                  required={true}
                />
          </div>
          <div className="col-md-4 mt-15">
              <FLInput
                  label={language.bank_account}
                  value={bank_account}
                  changeEvent={(value)=>{
                    setBankAccount(value)
                  }}
                  required={true}
                  isUpperCase={true}
                />
          </div>
        </div>}


        {(transfer_type=="STK" && bank_type == "QT")&&<div className="row bank_quocte">
          <div className="col-md-6 col-sm-6 col-lg-3 mt-15">
            <FLInput
                label={language.bene_bank_name}
                value={bank_name_qt}
                changeEvent={(value)=>{
                    setBankNameQT(value)
                  }}
                required={true}
              />
          </div>
          <div className="col-md-6 col-sm-6 col-lg-3 mt-15">
            <FLInput
                label={language.swiffcode}
                value={bank_swiffcode}
                icon={"fa-exclamation-circle"}
                changeEvent={(value)=>{
                    setBankSwiffcode(value)
                }}
                required={true}
              />
          </div>
          <div className="col-sm-12 col-md-12 col-lg-6 mt-15">
            <FLInput
                label={language.address_bank}
                value={bank_address}
                changeEvent={(value)=>{
                  setBankAddress(value)
                }}
                required={false}
              />
          </div>
          
          <div className="col-md-6 mt-15">
            <FLInput
                label={language.bank_number}
                value={bank_number}
                changeEvent={(value)=>{
                    setBankNumber(value)
                }}
                required={true}
              />
          </div>
          <div className="col-md-6 mt-15">
            <FLInput
                label={language.bank_account}
                value={bank_account}
                changeEvent={(value)=>{
                    setBankAccount(value)
                }}
                isUpperCase={true}
                required={true}
              />
          </div>
          <div className="col-md-12 mt-15">
            <FLInput
                label={language.address_acount}
                value={account_address}
                changeEvent={(value)=>{
                  setAccountAddress(value)
                }}
                required={true}
              />
          </div>

          <div className="col-md-6 mt-15">
            <FLSwitch
                label={language.pay_via_middle_bank}
                value={isMiddle}
                changeEvent={(value)=>{
                  setMiddle(value)
                }}
              />
          </div>
         


        </div>}

        {(transfer_type=="STK" && bank_type == "QT" && isMiddle)&&<div className="row">
            <div className="col-xs-12 col-sm-6 col-lg-4 col-md-6 mt-15">
            <FLInput
                label={language.middle_bank_name}
                value={middle_bank_name}
                changeEvent={(value)=>{
                  setMiddleBankName(value)
                }}
                required={true}
              />
          </div>
          <div className="col-xs-12 col-sm-6 col-lg-4 col-md-6 mt-15">
            <FLInput
                label={language.swiffcode}
                value={middle_swiff}
                changeEvent={(value)=>{
                    setMiddleSwiff(value)
                }}
                icon={"fa-exclamation-circle"}
                required={true}
              />
          </div>
          </div>}




    </div>
  );
});

export default BankRender;
