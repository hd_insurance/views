//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import styles from "../style.module.css";
import { isServer } from "../platform.js";

const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(props.value);
  if (isServer) return null;

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  function handleChange(e, label) {
    const target = e.target;
    setValue(target.checked);
    if (props.changeEvent) {
      props.changeEvent(target.checked);
    }
    // console.log(label, target.checked);
  }

  return (
    <div className={`${props.extend_class}`}>
      <label className={styles.form_switch}>
        <input
          type="checkbox"
          name={"newCar"}
          checked={value}
          onChange={handleChange}
        />
        <i></i> {props.label}
      </label>
    </div>
  );
});

export default Main;
