//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import styles from "../style.module.css";
import { isServer } from "../platform.js";
import randomstring from "randomstring";

const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(props.value || "");
  const rdt = randomstring.generate(7);
  const [loading, setLoading] = useState(false);
  if (isServer) return null;

  useEffect(() => {
    setValue(props.value);
    // console.log(props.value)
  }, [props.value, props.data]);

  function handleChange(e, label) {
    const target = e.target;
    setValue(target.value);
    if (props.changeEvent) {
      props.changeEvent(target.value);
    }
    // console.log(label, target.value);
  }

  return (
    <div className={"d-md-flex"}>
      {props.label && (
          <p className={styles.get_vat}>{props.label || ""}</p>
      )}
      <div className={styles.rd_group}>
        {props.data.map((item, i) => {
          return (
            <div className={styles.radio_wrap} key={i}>
              <input
                className="radio"
                type="radio"
                name={`${props.template_id}_${rdt}`}
                id={`${props.template_id}-${rdt}-${i}`}
                value={item.value}
                onChange={(e) => handleChange(e, item.label)}
                checked={value == item.value}
              />
              <label htmlFor={`${props.template_id}-${rdt}-${i}`}>{item.label}</label>
            </div>
          );
        })}
      </div>
    </div>
  );
});

export default Main;
