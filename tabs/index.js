import React, { useEffect, useState, forwardRef } from 'react';
import { Button, Form, Row, Col, Tabs, Tab, Nav } from 'react-bootstrap';
import Skeleton from 'react-loading-skeleton';
import styles from '../style.module.css';


const TabsBst = (props) => {
 const [key, setKey] = useState(props.current_tab?props.current_tab:"0");


 useEffect(() => {
    setKey(props.current_tab)
  }, [props.current_tab]);

  return (
  <div className={props.config.tab_class}>
     <Tab.Container 
      defaultActiveKey={props.defaultActiveKey}
      activeKey={key}
      onSelect={(key_active)=>{
        setKey(key_active)
        if(props.onTabSelect){
          console.log("key = ", key_active)
          props.onTabSelect(key_active)
        }
      }}>
      <div>
       
        <div className="tab-bst-content">
         {props.children}
        </div>
      </div>
    </Tab.Container>
  </div>
  );
};

export default TabsBst;
