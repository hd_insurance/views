//        ######################################
//        #     Floating Pophover              #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState } from "react";
import api from "../services/Network";
import StageSpinner from "../loading/index.js";

import styles from '../style.module.css';
import {isServer} from "../platform.js";


var listCache = {}
const hour_arr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
const minute_arr = [0, 5, 10, 15,20,25,30,35,40,45,50,55]

const Main = (props) => { 
  if (isServer) return null;
  const [h, setH] = useState(0);
  const [m, setM] = useState(0);
  
  useEffect(() => {
    let isSubscribed = true
    if (isSubscribed) {
     
    }
    return () => isSubscribed = false
  },[props]);

  useEffect(() => {
    if(props.value){
      const j = props.value.split(":")
      if(j[0]*1){
        setH(j[0]*1)
      }
      if(j[1]*1){
        setM(j[1]*1)
      }
    }
  },[props.value]);


  const upHour = ()=>{
    if(h<23){
      setH(h+1)
    }else{
      setH(0)
    }
  }
  const upMin = ()=>{
    if(m<55){
      setM(m+5)
    }else{
      upHour()
      setM(0)
    }
  }
  const downHour = ()=>{
    if(h==0){
      setH(23)
    }else if(h>0){
      setH(h-1)
    }else{
      setH(0)
    }
  }
  const downMin = ()=>{
    if(m>0){
      setM(m-5)
    }else{
      downHour()
      setM(55)
    }
  }
  const setTime = ()=>{
    let time_result = `${h<10?"0"+h:h}:${m<10?"0"+m:m}`;
    props.onSettime(time_result)
  }
 
  return (
    <div className={styles.float_pophover_time}>
        <table>
          <tr className={styles.title}>
            <td colspan="2">Giờ</td>
            <td colspan="1"></td>
            <td colspan="2">Phút</td>
          </tr>
          <tr>
            <td colspan="2"><i class="fa fa-caret-up" aria-hidden="true" onClick={()=>upHour()}></i></td>
            <td colspan="1"></td>
            <td colspan="2"><i class="fa fa-caret-up" aria-hidden="true" onClick={()=>upMin()}></i></td>
          </tr>
          <tr>
            <td colspan="2">{h<10?(`0${h}`):h}</td>
            <td colspan="1">:</td>
            <td colspan="2">{m<10?(`0${m}`):m}</td>
          </tr>
          <tr>
            <td colspan="2"><i class="fa fa-caret-down" onClick={()=>downHour()} aria-hidden="true"></i></td>
            <td colspan="1"></td>
            <td colspan="2"><i class="fa fa-caret-down" onClick={()=>downMin()} aria-hidden="true"></i></td>
          </tr>
        </table>

        <div className={styles.action_tb}>
          <button type="button" className={styles.btn_timer_confirm} onClick={()=>setTime()}>Set</button>
        </div>
    </div>
  );
};

export default Main;
