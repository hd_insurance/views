//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import Skeleton from "react-loading-skeleton";
import NumberFormat from "react-number-format";
import styles from "../style.module.css";
import { isServer } from "../platform.js";
import Popover from "react-popover";
import PopSelect from "./pop-timer.js";
import WrapSelect from "../common/wrappopup.js";


const TimePickerInput = forwardRef((props, ref) => {
  const [value, setValue] = useState(null);
  const [openTimepicker, setOpenTimepicker] = useState(false);
  
  const [isActive, setIsActive] = useState(
    props.value != null && props.value != ""
  );
  const [loading, setLoading] = useState(false);

  if (isServer) return null;

  React.useEffect(() => {
    if (props.value != value) {
      setValue(props.value);
    }
  }, [props.value]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  const popoverPropsAddress = {
    isOpen: openTimepicker,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenTimepicker(false),
    body: [
      <PopSelect value={value} onSettime={(v)=>{
        setValue(v)
        setOpenTimepicker(false)
      }}/>,
    ],
  };
  const handleClick = () => {
    !props.disable && setOpenTimepicker(true);
  };


  function handleTextChange(text) {
    if (props.changeEvent && !props.disable) {
      props.changeEvent(text.formattedValue);
      setValue(text.formattedValue);
    }

    if (text.formattedValue !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }

  const onIconClick = () => {
    if (props.onIconClick) {
      props.onIconClick();
    }
  };

  function limit(val, max) {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "00";

        //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  }

  function timeFormat(val) {
    let hours = limit(val.substring(0, 2), "23");
    let minutes = limit(val.substring(2, 4), "59");

    return hours + (minutes.length ? ":" + minutes : "");
  }

  var className = `${styles.formater} form-control`;
  if (props.hideborder) {
    className = `${className} ${styles.no_border}`;
  }
  if (props.icon) {
    className = `${className} ${styles.has_icon}`;
  }
  if (props.isUpperCase) {
    className = `${className} ${styles.has_uppercase}`;
  }
  if (props.line) {
    className = `${className} ${styles.text_line_through}`;
  }
  if (props.rightPosition) {
    className = `${className} ${styles.no_border_left}`;
  }
  return (
    <div
      id={styles.float_label}
      className={props.disable ? styles.ipt_disable : ""}
    >


      {props.loading ? <Skeleton width={"100%"} /> : null}

      <Popover {...popoverPropsAddress}>
      <WrapSelect handleClick={handleClick}>
        <NumberFormat
          className={className}
          // format="##/##/####"
          placeholder="hh:mm"
          mask={["h", "h", "m", "m"]}
          onValueChange={(v) => handleTextChange(v)}
          onFocus={(v)=>{
            handleClick()
          }}
          value={value}
          format={timeFormat}
          required={true}
        />

        <div className={styles.icon} onClick={() => onIconClick()}>
          <i className={`${styles.fa} ${styles.fa_clock}`} aria-hidden="true"></i>
        </div>
      </WrapSelect>
      </Popover>


    


      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i
            className={`${styles.fa} ${styles.fa_caret_down}`}
            aria-hidden="true"
          ></i>
        </div>
      ) : null}
      <label
        className={
          isActive || (props.value != null && props.value != "")
            ? styles.Active
            : ""
        }
        htmlFor="inp"
      >
        {props.label || ""}{" "}
        {props.required ? <span style={{ color: "#DA2128" }}>*</span> : ""}
      </label>
    </div>
  );
});

export default TimePickerInput;
