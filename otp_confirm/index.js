import React, {
  useEffect,
  useState,
  forwardRef,
  useRef,
  useImperativeHandle
} from "react";
import FLTextArea from "../fltextarea";
import FLInput from "../flinput";
import UploadItem from "../uploadframe/UploadItem";
import style from "../style.module.css"
import api from "../services/Network";
import cogoToast from 'cogo-toast';
import LoadingComponent from "../loadingComponent";

const STATUS = {
  STARTED: 'Started',
  STOPPED: 'Stopped',
}
const INITIAL_COUNT = 90

const default_lang = {
  reason_lb: "Nhập lý do không đồng ý với phương án bồi thường và file đính kèm",
  mail_1: "Vui lòng ấn nút lấy mã và nhập mã xác nhận được gửi theo email ",
  mail_2: " để hoàn tất xác nhận phương án bồi thường.",
  get_otp: "Lấy mã",
  otp_label: "Mã xác thực",
  reason: "Lý do"
}



const OtpConfirm = forwardRef((props, ref) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isShowReason, setShowReason] = useState(props.otpConfig.showReason);
  const [contact, setContact] = useState(props.otpConfig.contact);
  const [language, setLanguage] = useState(props.language?props.language:default_lang);
  const [otp, setOtp] = useState("");
  const [reason, setReason] = useState(null);
  const [secondsRemaining, setSecondsRemaining] = useState(INITIAL_COUNT)
  const [status, setStatus] = useState(STATUS.STOPPED)
  const secondsToDisplay = secondsRemaining % 60
  const minutesRemaining = (secondsRemaining - secondsToDisplay) / 60
  const minutesToDisplay = minutesRemaining % 60
  const hoursToDisplay = (minutesRemaining - minutesToDisplay) / 60


    useImperativeHandle(ref, () => ({ 
      getOtpData() {
        if(otp.length>0){
          return {
            otp: otp,
            reason: reason
          }
        }else{
          cogoToast.error("Please input otp")
          return false
        }
      }

    }))



  useEffect(() => {
    setShowReason(props.otpConfig.showReason)
  }, [props.otpConfig]);



  const handleStart = () => {
    setStatus(STATUS.STARTED)
  }
  const handleStop = () => {
    setStatus(STATUS.STOPPED)
  }
  const handleReset = () => {
    setStatus(STATUS.STOPPED)
    setSecondsRemaining(INITIAL_COUNT)
  }
  useInterval(
    () => {
      if (secondsRemaining > 0) {
        setSecondsRemaining(secondsRemaining - 1)
      } else {
        setStatus(STATUS.STOPPED)
      }
    },
    status === STATUS.STARTED ? 1000 : null,
    // passing null stops the interval
  )

function useInterval(callback, delay) {
  const savedCallback = useRef()

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current()
    }
    if (delay !== null) {
      let id = setInterval(tick, delay)
      return () => clearInterval(id)
    }
  }, [delay])
}
  const twoDigits = (num) => String(num).padStart(2, '0')


  const onFileUpload = ()=>{

  }
  const onFileDelete = ()=>{
    
  }

  const getOtp = async ()=>{
    if(status == STATUS.STOPPED){
      
      try{
        setIsLoading(true)
        const otpdata = {claimCode: props.otpConfig.claimCode}
        const send_result = await api.post("/api/eclaim/get-otp", otpdata)
        setIsLoading(false)
        if(send_result.Success){
          setSecondsRemaining(INITIAL_COUNT)
          setStatus(STATUS.STARTED)
          cogoToast.info(send_result.Data.Message);
        }else{
          cogoToast.error(send_result.Data.Message);
        }
      }catch(e){
        console.log("OTP ERROR ", e)
        setIsLoading(false)
      }

    }else{

    }
    
  }


  return (
    <div className={`${style.otp_cf_container} ${props.className}`}>
    <LoadingComponent loading={isLoading}>
      <div className={style.otp_icon_cls}>
        <img className={style.img_otp_icon} src="/img/eclaim/otp_input.png"/>
      </div>
      {isShowReason?<div className={style.reason_class}>
        
        <p>{language.reason_lb}</p>
        <FLTextArea
          label={language.reason}
          row={5}
          value={reason}
          changeEvent={setReason}
        />

          <div className="upload-frame-container">
          <div className="upload-f-item">
          <div className="upload-body">
          <div className="upload-ctx2">
          <div className="file-item">
            <UploadItem viewOnly={false} data={{}} onFileUpload={onFileUpload} onFileDelete={onFileDelete} language={language} />
            </div>
            </div>
            </div>
          </div>
          </div>

      </div>:null}


      <div className={style.otp_input}>
        <p>{language.mail_1}<b>{contact}</b>{language.mail_2}</p>
        <div className={style.otp_abhm}>
          <FLInput
            label={language.otp_label}
            required={true}
            extend_class={style.input_n_otp}
            value={otp}
            changeEvent={setOtp}
          />
          <button className={`${(status == STATUS.STOPPED)?style.otp_active:style.otp_none} ${style.btn_send_otp}`} onClick={getOtp}>{(status == STATUS.STOPPED)?language.get_otp:`${twoDigits(minutesToDisplay)}:${twoDigits(secondsToDisplay)}`}</button>

        </div>
      </div>


      </LoadingComponent>
    </div>
  );
});

export default OtpConfirm;
