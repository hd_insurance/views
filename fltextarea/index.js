//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from 'react';
import Skeleton from 'react-loading-skeleton';
import styles from '../style.module.css';
import {isServer} from "../platform.js";

const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(props.value);
  const [isActive, setIsActive] = useState(props.value != null);
  
  if (isServer) return null;

  React.useEffect(() => {
    setValue(props.value);
  }, [props.value]);
  function handleTextChange(text) {
    props.changeEvent(text);
    setValue(text);
    if (text !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }
  return (
    <div
      id={styles.float_label}
      className={props.disable ? styles.ipt_disable : ''}
    >
      {props.loading ? <Skeleton width={'100%'} height={'100%'} /> : null}
      <textarea
        type='text'
        className={styles.fl_text_area}
        rows={props.row?props.row:'2'}
        disabled={props.disable}
        value={value || ''}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        className={props.hideborder ? styles.no_border : ''}
        onChange={(e) => handleTextChange(e.target.value)}
      />
      {props.icon ? (
        <div className={styles.icon}>
          <i className={`${styles.fa} ${props.icon}`} aria-hidden='true'></i>
        </div>
      ) : null}
      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i
            className={`${styles.fa} ${styles.fa_caret_down}`}
            aria-hidden='true'
          ></i>
        </div>
      ) : null}
      <label
        className={isActive || props.value != null ? styles.Active : ''}
        htmlFor='inp'
      >
        {props.label || ''}{' '}
        {props.required ? <span style={{ color: '#DA2128' }}>*</span> : ''}
      </label>
    </div>
  );
});

export default Main;
