//        ######################################
//        #     Floating Pophover              #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState } from 'react';
import styles from '../style.module.css';
import {isServer} from "../platform.js";

var listCache = {};
const Main = (props) => {
  const [loading, setLoading] = useState(true);
  const [isBack, setBack] = useState(false);
  const [list, setList] = useState([]);

  const [step, setStep] = useState(0);
  const [stitle, setStitle] = useState(props.title);
  const [subbtitle, setSubStitle] = useState('');
  const [clL1, setClL1] = useState('ul-select');
  if (isServer) return null;

  useEffect(() => {
    let isSubscribed = true;
    if (isSubscribed) {
      if (list.length == 0) {
        list.push(props.relationList);
        setList([...list]);
      }
    }
    return () => (isSubscribed = false);
  }, [props]);

  const onItemClick = (item) => {
    if (item.list) {
      setBack(false);
      setStitle(item.subtitle);
      setSubStitle(item.title);
      setStep(step + 1);
      list.push(item.list);
      setList([...list]);
    } else {
      props.selected(item);
    }
  };

  const back = () => {
    if (step > 0) {
      setBack(true);
      // list.splice((list.length - 1), 1);
      setStep(step - 1);
      setStitle(props.title);
      setTimeout(() => {
        list.splice(list.length - 1, 1);
        setList([...list]);
      }, 800);
      // setList([...list])
      //  console.log(list.length)
    }
  };

  return (
    <div className={`${styles.float_pophover} ${styles.md}`}>
      <div className={styles.popheader}>
        {step > 0 ? (
          <div>
            <div
              className={`${styles.backpopup} ${styles.animate__animated} ${styles.animate__bounceIn}`}
              onClick={() => back()}
            >
              <i className={`${styles.fa} ${styles.fa_chevron_left}`} aria-hidden='true'></i>
            </div>
            <p
              className={`${styles.country_name} ${styles.animate__animated} ${styles.animate__fadeInDown}`}
            >
              {subbtitle}
            </p>
          </div>
        ) : null}
        <p
          className={
            stitle != props.title
              ? `${styles.stitle} ${styles.animate__animated} ${styles.animate__fadeInUp}`
              : `${styles.stitle} ${styles.animate__animated} ${styles.animate__fadeInDown}`
          }
        >
          {stitle}
        </p>
      </div>
      <div className={`${styles.select_body} ${styles.hzld}`}>
        {list.map((child_list, ix) => {
          var classShow = `${styles.ul_select} ${styles.absolute} ${styles.animate__animated} ${styles.animate__backInRight}`;
          var classHide = `${styles.ul_select} ${styles.absolute} ${styles.animate__animated} ${styles.animate__backOutLeft}`;
          if (isBack) {
            classShow = `${styles.ul_select} ${styles.absolute} ${styles.animate__animated} ${styles.animate__backInLeft}`;
            classHide = `${styles.ul_select} ${styles.absolute} ${styles.animate__animated} ${styles.animate__backOutRight}`;
          }
          return (
            <ul key={ix} className={step == ix ? classShow : classHide}>
              {child_list.map((item, index) => {
                return (
                  <li
                    key={index}
                    className={styles.sx_item}
                    onClick={(e) => onItemClick(item)}
                  >
                    {item.title}
                  </li>
                );
              })}
            </ul>
          );
        })}
      </div>
    </div>
  );
};

export default Main;
