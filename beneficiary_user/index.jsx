import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import FLInput from "../flinput";
import FLSelect from "../flselect";
import FLSwitch from "../flswitch";
import FLBankList from "../flbanklist";
import api from "../services/Network";

const default_language = {
  "text_beneciary": "Người thụ hưởng",
  "text_gender": "Danh xưng",
  "text_bene_name": "Tên người thụ hưởng",
  "text_bene_phone": "Số điện thoại",
  "text_bene_passport": "CMND/ CCCD/ Hộ chiếu"
}
const gender_data = [
    {
      label: "Ông (Mr)",
      value: "ONG"
    },
     {
      label: "Bà (Ms)",
      value: "BA"
    }
  ]


const BankRender = forwardRef((props, ref) => {
  const [language, setLanguage] = useState(props.language?props.language:default_language);
  const [data, setData] = useState([]);
  const [bene_type, setBeneType] = useState("");
  const [bene_label, setBeneLabel] = useState("");
  const [gender, setGender] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [passport, setPassport] = useState("");


  const onInfoChange = ()=>{
    return {

    }
  }

  useEffect(() => {
    getBeneciaryType()
  }, []);


   useImperativeHandle(ref, () => ({ 

      getData() {
        if(bene_type == "INSURER"){
          return {
            bene_type: bene_type,
            bene_label: bene_label,
            gender: "",
            name: "",
            phone: "",
            passport: ""
          }
        }else{
          return {
            bene_type: bene_type,
            bene_label: bene_label,
            gender: gender,
            name: name,
            phone: phone,
            passport: passport
          }
        }
      }

    }))

  useEffect(() => {
    if(props.data){
      setData(props.data)
    }
  },[props.data]);


  const getBeneciaryType = ()=>{
    return new Promise(async (resolved, rejected)=>{
      try {
        const response = await api.get(`/api/eclaim/product-list/define-key/BENEFICIARY_TYPE`);
        var list_beneciary = []
        response.data[0].forEach((item, index)=>{
          list_beneciary.push({
            label: item.TYPE_NAME,
            value: item.TYPE_CODE
          })

        })

        const myArray = list_beneciary.filter(function( obj ) {
           return obj.value == "INSURER";
        });
        if(myArray[0]){
          setBeneLabel(myArray[0].label)
          setBeneType("INSURER")
        }
        
        setData(list_beneciary)
        
        
      }catch(e){
        console.log("co loi xay ra ",e)
        return resolved(true)
      }
    })
     

  }

  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };

  //changeEvent

  return (
    <div className={`${props.extend_class}`} onClick={handleOnclick}>
        <div className="row">
          
          <div className="col-md-4  mt-15">
            <FLSelect
                  data={data}
                  value={bene_type}
                  label={language.text_beneciary}
                  required={true}
                  changeEvent={(value)=>{
                    const myArray = data.filter(function( obj ) {
                        return obj.value == value;
                    });
                    if(props.onBeneChange){
                      props.onBeneChange(value)
                    }
                    setBeneLabel(myArray[0].label)
                    setBeneType(value)
                  }}
                />
          </div>

          {(bene_type == "BENEFICIARY_POINT" || bene_type == "HEIR")&&<div className="col-md-4 mt-15">
            <FLSelect
                  data={gender_data}
                  value={gender}
                  label={language.text_gender}
                  required={true}
                  changeEvent={(value)=>{
                    setGender(value)
                  }}
                />
          </div>}

          {(bene_type == "BENEFICIARY_POINT" || bene_type == "HEIR")&&<div className="col-md-4  mt-15">
            <FLInput
                  value={name}
                  label={language.text_bene_name}
                  isUpperCase={true}
                  required={true}
                  changeEvent={(value)=>{
                    setName(value)
                  }}
                />
          </div>}

          {(bene_type == "BENEFICIARY_POINT" || bene_type == "HEIR")&&<div className="col-md-4  mt-15">
            <FLInput
                  value={phone}
                  label={language.text_bene_phone}
                  required={true}
                  changeEvent={(value)=>{
                    setPhone(value)
                  }}
                />
          </div>}

          {(bene_type == "BENEFICIARY_POINT" || bene_type == "HEIR")&&<div className="col-md-4  mt-15">
            <FLInput
                  value={passport}
                  label={language.text_bene_passport}
                  required={true}
                  changeEvent={(value)=>{
                    setPassport(value)
                  }}
                />
          </div>}

        </div>
    </div>
  );
});

export default BankRender;
