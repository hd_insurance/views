//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import NumberFormat from "react-number-format";
import styles from '../style.module.css';
import {isServer} from "../platform.js";


const Main = (props) => {
  const [value, setValue] = useState("");
  const [isActive, setIsActive] = useState((props.value != "" && props.value != null));
  const [loading, setLoading] = useState(false);

  if (isServer) return null;
  
  React.useEffect(() => {
    if (props.value != value) {
      setValue(props.value);
    }
  }, [props.value]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  function handleTextChange(text) {
    if (props.changeEvent) {
      if (text.value.length == 8) {
        props.changeEvent(text.formattedValue);
        setValue(text.formattedValue);
      }
    }

    if (text.formattedValue !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }

  const onIconClick = () => {
    if (props.onIconClick) {
      props.onIconClick();
    }
  };

  function limit(val, max) {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "01";

        //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  }

  function limityear(val, max) {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "01";

        //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  }

  function dateFormat(val) {
    let day = limit(val.substring(0, 2), "31");
    let month = limit(val.substring(2, 4), "12");
    let year = limityear(val.substring(4, 8), "2030");

    return (
      day + (month.length ? "/" + month : "") + (year.length ? "/" + year : "")
    );
  }

  var className = `${styles.formater} form-control`;
  if (props.hideborder) {
    className = `${className} ${styles.no_border}`;
  }
  if (props.icon) {
    className = `${className} ${styles.has_icon}`;
  }
  if (props.isUpperCase) {
    className = `${className} ${styles.has_uppercase}`;
  }
  if (props.line) {
    className = `${className} ${styles.text_line_through}`;
  }
  if (props.leftPosition) {
    className = `${className} ${styles.no_border_right}`;
  }
  return (
    <div id={styles.float_label} className={props.disable ? (`uikit_fldate_wrap ${styles.ipt_disable}`) : "uikit_fldate_wrap"}>
      {props.loading ? <Skeleton width={"100%"} /> : null}

      <NumberFormat
         className={`uikit_fldate ${className}`}
        // format="##/##/####"
        placeholder="DD/MM/YYYY"
        mask={["D", "D", "M", "M", "Y", "Y", "Y", "Y"]}
        onValueChange={(v) => handleTextChange(v)}
        value={value}
        format={dateFormat}
        onFocus={props.onFocus}
        required={props.required != undefined ? props.required : true}
        disabled={props.disable}
        readOnly={props.readOnly}
        onBlur={props.onBlur}
      />

      {props.icon ? (
        <div className={styles.icon} onClick={() => onIconClick()}>
          <i className={`${styles.fa} ${props.icon}`} aria-hidden="true"></i>
        </div>
      ) : null}
      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i className={`${styles.fa} ${styles.fa_caret_down}`} aria-hidden="true"></i>
        </div>
      ) : null}
      <label
        className={isActive || (props.value != "" && props.value != null) ? (`uikit_fldate_label ${styles.Active}`) : `uikit_fldate_label`}
        htmlFor="inp"
      >
        {props.label || ""}{" "}
        {props.required ? <span style={{ color: "#DA2128" }}>*</span> : ""}
      </label>
    </div>
  );
};

export default Main;
