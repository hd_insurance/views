import React, { useEffect, useState, forwardRef } from "react";
import Popover from "react-popover";
import PopSelectDate from "./datepicker.js";
import FLInput from "./format.jsx";
import styles from "../style.module.css";
import { isServer } from "../platform.js";
import WrapSelect from "../common/wrapselect.js";

const DateInput = forwardRef((props, ref) => {
  const [openDate, setOpenDate] = useState(false);
  const [dateValue, setDateValue] = useState(props.value);
  const [disable, setDisabled] = useState(props.disable);
  const [readonly, setReadonly] = useState(false);

  if (isServer) return null;

  useEffect(() => {
    setDateValue(props.value);
  }, [props.value]);


  useEffect(() =>{
    setDisabled(props.disable);
  },[props.disable])

  const dateSelect = (vl) => {
    if (vl) {
      setDateValue(
        `${vl.day < 10 ? `0${vl.day}` : vl.day}/${
          vl.month < 10 ? `0${vl.month}` : vl.month
        }/${vl.year}`
      );
    }
    setTimeout(() => {
      if (props.changeEvent) {
        props.changeEvent(
          `${vl.day < 10 ? `0${vl.day}` : vl.day}/${
            vl.month < 10 ? `0${vl.month}` : vl.month
          }/${vl.year}`
        );
      }

      setOpenDate(false);
    }, 200);
  };

  const formatdate = (indate) => {
    const a = indate.split("/");
    return {
      year: a[2] * 1,
      month: a[1] * 1,
      day: a[0] * 1,
    };
  };


  const popoverPropsDate = {
    isOpen: openDate,
    place: props.place || "below",
    preferPlace: "right",
    onOuterAction: () => setOpenDate(false),
    body: [
      <PopSelectDate
        selectedDay={dateValue ? formatdate(dateValue) : props.selectedDay}
        minimumDate={props.minimumDate}
        maximumDate={props.maximumDate}
        label={props.label}
        dateSelect={dateSelect}
      />,
    ],
  };

  const changeEvent = (data) => {
    setDateValue(data);
  };
  // const blurInput = () => {
  //   setReadonly(false);
  // };
  // const focusInput = () => {
  //   setOpenDate(true);
  //   setReadonly(true);
  // };
  const handleClick = () => {
    if(!disable){
      setOpenDate(true);
    }else return false
  };
  return (
    <Popover {...popoverPropsDate}>
      <WrapSelect handleClick={handleClick}>
        <FLInput
          loading={props.loading}
          readonly={true}
          label={props.label || "Ngày sinh"}
          // onFocus={(e) => focusInput()}
          // readOnly={readonly}
          value={dateValue}
          changeEvent={changeEvent}
          required={props.required != undefined ? props.required : true}
          icon={`${styles.far} ${styles.fa_calendar_alt}`}
          hideborder={props.hideborder}
          disable={disable}
          leftPosition={props.leftPosition}
          // onBlur={() => blurInput()}
        />
      </WrapSelect>
    </Popover>
  );
});
export default DateInput;
