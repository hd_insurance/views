//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import Skeleton from 'react-loading-skeleton';
import NumberFormat from 'react-number-format';
import styles from '../style.module.css';
import {isServer} from "../platform.js";
import Autosuggest from 'react-autosuggest';



const languages = [
    {
      name: 'Bệnh viện Bạch Mai',
      value: 1
    },
    {
      name: 'Bệnh viện E Hà Nội',
      value: 2
    },
    {
      name: 'Viện Mắt Trung ương',
      value: 3
    },
    {
      name: 'Bệnh viện Việt- Đức',
      value: 4
    },
    {
      name: 'Bệnh viện Hai Bà Trưng',
      value: 5
    },
    {
      name: ' Bệnh viện Bắc Thăng Long',
      value: 6
    },
    {
      name: 'Bệnh viện Đa khoa Gia Lâm',
      value: 7
    }
  ];




const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState("");
  const [value_item, setValueItem] = useState({label:"", value:""});
  const [suggestions, setSuggestions] = useState([]);
  const [_data, setData] = useState(props.data||languages);
  const [isActive, setIsActive] = useState(props.value != "" && props.value != null);
  const [loading, setLoading] = useState(false);
  if (isServer) return null;


  


const onChange = (event, { newValue }) => {
   setValue(newValue)
   if (newValue !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }


  };


const onBlur  = (event, { newvalue }) => {
    // value.label = newValue
    // setValue(value)
    
    if(value_item.label != event.target.value.trim()){

      // setValue({label: event.target.value, value:""})

      if (props.changeEvent) {
        props.changeEvent({label: event.target.value, value: ""});
      }
    }else{

    }
   
  
}

const inputProps = {
      placeholder: props.label,
      value: value,
      onChange: onChange,
      onBlur: onBlur
    };


  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
const  onSuggestionsFetchRequested = ({ value }) => {
   setSuggestions(getSuggestions(value));
  };

  // Autosuggest will call this function every time you need to clear suggestions.
const  onSuggestionsClearRequested = () => {
    setSuggestions([])
  }


  // Teach Autosuggest how to calculate suggestions for any given input value.
const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  return inputLength === 0 ? [] : _data.filter(lang =>
    removeAccents(lang.name.toString()).toLowerCase().includes(removeAccents(inputValue))  
  );
};

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.

const getSuggestionValue = suggestion => {
  
  if (props.changeEvent) {
    props.changeEvent({label: suggestion.name, value: suggestion.value});
  }
  setValueItem({label: suggestion.name, value: suggestion.value})
  return suggestion.name
};

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
  <div>
    {suggestion.name}
  </div>
);


  
  // React.useEffect(() => {
  //   if (props.value != value) {
  //     setValue(props.value);
  //   }
  // }, [props.value]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  useEffect(() => {
    if(props.data){
      setData(props.data);
    }
  }, [props.data]);

  function handleTextChange(text) {
    if (props.changeEvent) {
      props.changeEvent(text.value);
    }
    setValue(text.value);

    if (text.value !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }

  const onIconClick = () => {
    if (props.onIconClick) {
      props.onIconClick();
    }
  };



  const removeAccents = (str) => {
  var AccentsMap = [
    "aàảãáạăằẳẵắặâầẩẫấậ",
    "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
    "dđ", "DĐ",
    "eèẻẽéẹêềểễếệ",
    "EÈẺẼÉẸÊỀỂỄẾỆ",
    "iìỉĩíị",
    "IÌỈĨÍỊ",
    "oòỏõóọôồổỗốộơờởỡớợ",
    "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
    "uùủũúụưừửữứự",
    "UÙỦŨÚỤƯỪỬỮỨỰ",
    "yỳỷỹýỵ",
    "YỲỶỸÝỴ"    
  ];
  for (var i=0; i<AccentsMap.length; i++) {
    var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
    var char = AccentsMap[i][0];
    str = str.replace(re, char);
  }
  return str;
}


  var className = styles.ah;
  if (props.hideborder) {
    className = `${className} ${styles.no_border}`;
  }
  if (props.icon) {
    className = `${className} ${styles.has_icon}`;
  }
  if (props.isUpperCase) {
    className = `${className} ${styles.has_uppercase}`;
  }
  if (props.line) {
    className = `${className} ${styles.text_line_through}`;
  }
  return (
    <div id={styles.float_label} className={props.disable ? (styles.ipt_disable) : ''}>
      {props.loading ? <Skeleton width={'100%'} /> : null}

      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
      />

      {props.icon ? (
        <div className={styles.icon} onClick={() => onIconClick()}>
          <i className={`${styles.fa} ${props.icon}`} aria-hidden='true'></i>
        </div>
      ) : null}
      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i className={`${styles.fa} ${styles.fa_caret_down}`} aria-hidden='true'></i>
        </div>
      ) : null}
      <label
        className={
          isActive || (props.value != "" && props.value != null)
            ? `uikit_flinput_label ${styles.Active}`
            : `uikit_flinput_label`
        }
      >
        {props.label || ''}{' '}
        {props.required ? <span style={{ color: '#DA2128' }}>*</span> : ''}
      </label>
    </div>
  );
});

export default Main;
