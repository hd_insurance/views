import React, { useEffect, useState, forwardRef } from "react";
import Popover from "react-popover";
import PopSelect from "./pop-address.js";
import FLInput from "../flinput";
import styles from "../style.module.css";
import { isServer } from "../platform.js";
import WrapSelect from "../common/wrapselect.js";

const AddressInput = forwardRef((props, ref) => {
  const [openAddress, setOpenAddress] = useState(false);
  const [addressValue, setAddressValue] = useState(props.value?.label);
  if (isServer) return null;

  const locationFormSelect = (vl) => {
    setAddressValue(vl.label);
    if (props.changeEvent) {
      props.changeEvent(vl);
    }

    setTimeout(() => {
      setOpenAddress(false);
    }, 200);
  };

  useEffect(() => {
    setAddressValue(props.value?.label);
  }, [props.value?.label]);

  const popoverPropsAddress = {
    isOpen: openAddress,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenAddress(false),
    body: [
      <PopSelect mode={props.mode || 0} addressSelect={locationFormSelect} />,
    ],
  };

  // const handleTextChange = (e) => {
  //   console.log(e);
  //   return e.preventDefault();
  // };
  const handleClick = () => {
    !props.disable && setOpenAddress(true);
  };
  return (
    <Popover {...popoverPropsAddress}>
      <WrapSelect handleClick={handleClick}>
        <FLInput
          loading={props.loading}
          label={props.label}
          disableTyping={true}
          disable={props.disable}
          // onFocus={(e) => {
          //   !props.disable && setOpenAddress(true);
          // }}
          dropdown={props.dropdown || false}
          value={addressValue}
          required={props.required != undefined ? props.required : true}
          hideborder={props.hideborder}
          icon={
            props.dropdown ? false : `${styles.far} ${styles.fa_map_marker_alt}`
          }
        />
      </WrapSelect>
    </Popover>
  );
});
export default AddressInput;
