import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import styles from "../style.module.css";
import { ClassicSpinner } from "react-spinners-kit";


const DivRender = forwardRef((props, ref) => {
  const [loading, setLoading] = useState(false);
  const [effectLoading, setEffectLoading] = useState(false);
  useEffect(() => {
    if(props.loading){
      setLoading(true)
      setEffectLoading(true)
    
    }else{
      setEffectLoading(false)
      setTimeout(
        () => setLoading(false), 
        200
      );
    }
  }, [props.loading]);

  return (
    <div className={`${loading?styles.is_loading:"none_loading"}`}>
    <div className={`${styles.ld_cpn_ctn} ${effectLoading?styles.m_fadeIn:styles.m_fadeOut}`}>
      <div className={styles.lg_iste}>
        <ClassicSpinner size={30} color="#686769" loading={loading} color={"#329945"} size={50}/>
      </div>
    </div>
      {props.children}
    </div>
  );
});

export default DivRender;
