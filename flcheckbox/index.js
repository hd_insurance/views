//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import styles from "../style.module.css";
import { isServer } from "../platform.js";
import randomstring from "randomstring";


const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(props.value);
  const rdt = randomstring.generate(7);

  if (isServer) return null;

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  function handleChange(e, label) {
    const target = e.target;
    setValue(target.checked)
    if (props.changeEvent) {
      props.changeEvent(target.checked);
    }
    // console.log(label, target.checked);
  }

  return (
    <div className={`${props.extend_class}`}>
      <input className="checkbox" type="checkbox" id={`${props.template_id}-${rdt}`} checked={value} onChange={(e)=>handleChange(e)} disabled={props.disable}/>
      <label htmlFor={`${props.template_id}-${rdt}`}>
        <p>{props.label}</p>
      </label>
    </div>
  );
});

export default Main;
