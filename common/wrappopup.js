import React, { useEffect, useState, forwardRef } from "react";
import styles from "../style.module.css";

const WrapSelect = forwardRef((props, ref) => {
  return (
    <div className={styles.wrap_cs_select}>
      
      {props.children}
    </div>
  );
});

export default WrapSelect;
