import React, { useEffect, useState, forwardRef } from "react";
import styles from "../style.module.css";

const WrapSelect = forwardRef((props, ref) => {
  return (
    <div className={styles.wrap_cs_select}>
      <div
        onClick={() => props.handleClick&&props.handleClick()}
        className={styles.mask_flselect}
      ></div>
      {props.children}
    </div>
  );
});

export default WrapSelect;
