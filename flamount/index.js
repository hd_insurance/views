//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import Skeleton from 'react-loading-skeleton';
import NumberFormat from 'react-number-format';
import styles from '../style.module.css';
import {isServer} from "../platform.js";

const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(null);
  const [isActive, setIsActive] = useState(props.value != "" && props.value != null);
  const [loading, setLoading] = useState(false);
  if (isServer) return null;
  
  React.useEffect(() => {
    if (props.value != value) {
      setValue(props.value);
    }
  }, [props.value]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  function handleTextChange(text) {
    if (props.changeEvent) {
      props.changeEvent(text.value);
    }
    setValue(text.value);

    if (text.value !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }

  const onIconClick = () => {
    if (props.onIconClick) {
      props.onIconClick();
    }
  };

  var className = styles.ah;
  if (props.hideborder) {
    className = `${className} ${styles.no_border}`;
  }
  if (props.icon) {
    className = `${className} ${styles.has_icon}`;
  }
  if (props.isUpperCase) {
    className = `${className} ${styles.has_uppercase}`;
  }
  if (props.line) {
    className = `${className} ${styles.text_line_through}`;
  }
  return (
    <div id={styles.float_label} className={props.disable ? (styles.ipt_disable) : ''}>
      {props.loading ? <Skeleton width={'100%'} /> : null}

      <NumberFormat
        className={`${styles.formater} form-control`}
        placeholder='0 VNĐ'
        onValueChange={(v) => handleTextChange(v)}
        value={value}
        thousandSeparator={true}
        suffix={' VNĐ'}
        disabled={props.disable}
        onFocus={props.onFocus}
        required={true}
      />

      {props.icon ? (
        <div className={styles.icon} onClick={() => onIconClick()}>
          <i className={`${styles.fa} ${props.icon}`} aria-hidden='true'></i>
        </div>
      ) : null}
      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i className={`${styles.fa} ${styles.fa_caret_down}`} aria-hidden='true'></i>
        </div>
      ) : null}
      <label
        className={
          isActive || (props.value != "" && props.value != null)
            ? `uikit_flinput_label ${styles.Active}`
            : `uikit_flinput_label`
        }
      >
        {props.label || ''}{' '}
        {props.required ? <span style={{ color: '#DA2128' }}>*</span> : ''}
      </label>
    </div>
  );
});

export default Main;
