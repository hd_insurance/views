//        ######################################
//        #     Floating Label input           #
//        #     HoangPM                        #
//        ######################################

import React, { useEffect, useState, forwardRef } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import Skeleton from "react-loading-skeleton";
import styles from "../style.module.css";
import { isServer } from "../platform.js";

const Main = forwardRef((props, ref) => {
  const [value, setValue] = useState(props.value);
  const [isActive, setIsActive] = useState(
    props.value != "" && props.value != null
  );
  const [loading, setLoading] = useState(false);
  if (isServer) return null;

  useEffect(() => {
    setValue(props.value);
    if (props.value == "") {
      setIsActive(false);
    }
  }, [props.value]);

  // useEffect(() => {
  //   if(props.component_obj){
  //     console.log("props.component_obj.define.value ", props.component_obj.define.value)
  //   }
  // }, [props.component_obj]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  function handleTextChange(e) {
    var text = e.target.value;
    if (props.disableTyping) {
      return e.preventDefault();
    }

    if (!/^[0-9]*$/g.test(text) && props.disableDecima) {
      return e.preventDefault();
    } // chan nhap so thap phan
    setValue(text);
    if (props.changeEvent) {
      props.changeEvent(text);
    }
    if (text !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }

  var className = styles.ah + " " + props.position || null;
  if (props.hideborder) {
    className = `${className} ${styles.no_border}`;
  }
  if (props.icon) {
    className = `${className} ${styles.has_icon}`;
  }
  if (props.isUpperCase) {
    className = `${className} ${styles.has_uppercase}`;
  }
  if (props.line) {
    className = `${className} ${styles.text_line_through}`;
  }
  return (
    <div
      id={styles.float_label}
      className={
        props.disable
          ? `uikit_flinput_wrap ${styles.ipt_disable} ${styles.css_width_input} ${props.extend_class}`
          : `uikit_flinput_wrap ${styles.css_width_input} ${props.extend_class}`
      }
    >
      {loading ? <Skeleton width={"100%"} /> : null}

      <Form.Control
        type={props.type ? props.type : "text"}
        readOnly={props.readonly}
        disabled={props.disable}
        value={value || ""}
        placeholder={props.placeholder || ""}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        className={`uikit_flinput ${className}`}
        required={props.required}
        pattern={props.pattern || null}
        autoComplete="false"
        onChange={(e) => handleTextChange(e)}
      />

      {props.icon ? (
        <div className={styles.icon}>
          <i className={`${styles.fa} ${props.icon}`} aria-hidden="true"></i>
        </div>
      ) : null}
      {props.dropdown ? (
        <div className={styles.dropdown}>
          <i className={`fa fa-caret-down`} aria-hidden="true"></i>
        </div>
      ) : null}
      <label
        className={
          isActive || (props.value != "" && props.value != null)
            ? `uikit_flinput_label ${styles.Active}`
            : `uikit_flinput_label`
        }
        htmlFor="inp"
      >
        {props.label || ""}{" "}
        {props.required ? <span style={{ color: "#DA2128" }}>*</span> : ""}
      </label>
    </div>
  );
});

export default Main;
