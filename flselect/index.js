import React, { useEffect, useState, forwardRef } from "react";
import Popover from "react-popover";
import FLInput from "../flinput";
import { isServer } from "../platform.js";
import styles from "../style.module.css";
import WrapSelect from "../common/wrapselect.js";

const ListedInput = forwardRef((props, ref) => {
  const [listData, setListData] = useState([]);
  const [openList, setOpenList] = useState(false);
  const [disable, setDisabled] = useState(props.disable);
  const [value, setValue] = useState({
    label: "",
    value: props.value || "",
  });

  const [searchVL, setSearchVL] = useState("");

  const [readonly, setReadonly] = useState(false);

  if (isServer) return null;

  useEffect(() => {
    if (props.data) {
      setListData(props.data);
    }
  }, [props.data]);

   useEffect(() =>{
    setDisabled(props.disable);
  },[props.disable])

  useEffect(() => {
    if (props.component_obj) {
      props.component_obj.onDataset = (data) => {
        setListData(data);
      };
      props.component_obj.onValueset = (value) => {
        if (value) {
          setValue(value);
        }
      };
    }
  }, []);

  useEffect(() => {
    let result = listData.filter((obj) => {
      return obj.value === props.value;
    });

    if (result[0]) {
      setValue(result[0]);
    } else {
      setValue({ label: "", value: "" }); // truong hop value truyen vao k co trong dlist data thi set mac dinh
    }
  }, [props.value, listData]);

  const removeAc = (str) => {
    var AccentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ", "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ"
    ];
    for (var i=0; i<AccentsMap.length; i++) {
      var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
      var char = AccentsMap[i][0];
      str = str.replace(re, char);
    }
    return str;
  }

  const filterFunction = (name, value) =>{
    const l1 = removeAc(name.toString()).toLowerCase()
    const l2 = removeAc(value).toLowerCase()
    return (l1.includes(l2))
  }
  const onsearchChange = (e)=>{
    setSearchVL(e.target.value);
    var filtered = props.data.filter(obx => {
      const vl = obx.label
      return filterFunction(vl, e.target.value)
    });
    setListData(filtered);
  }

  const popoverProps = {
    isOpen: openList,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenList(false),
    body: [
      <div>
        <div className="popsheader">
          {props.label}
          {props.is_search && (
              <div className={styles.search_select_cus}>
                <input type="text" placeholder="Nhập để tìm kiếm" value={searchVL} onChange={onsearchChange} />
                <i className="fas fa-search"/>
              </div>
          )}
        </div>
        <div className={`list-item ${styles.custom_list_year}`}>
          {listData.map((item, index) => {
            return (
              <div
                key={index}
                className={
                  item.value == value.value ? "sx-item active" : "sx-item"
                }
                onClick={(e) => {
                  if (props.changeEvent) {
                    // console.log("da vao flselect", props.changeEvent)
                    const check = props.changeEvent(item.value, index);
                    setOpenList(false);
                    if (check == "stopPropagation") return;
                  }
                  setValue(item);
                  setOpenList(false);
                }}
              >
                {item.label}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };
 
  // const focusInput = () => {
  //   setOpenList(true);
  //   setReadonly(true);
  // };
  const handleClick = () => {
    if(!disable){
      setOpenList(true);
    }else return false
  };
  return (
    <Popover {...popoverProps}>
      <WrapSelect handleClick={handleClick} >
        <FLInput
          disable={props.disable}
          readonly={readonly}
          position={props.position}
          loading={props.loading}
          label={props.label}
          hideborder={props.hideborder}
          // onFocus={(e) => focusInput()}
          value={value.label}
          required={props.required}
          dropdown={true}
          // onBlur={() => blurInput()}
          extend_class={props.extend_class || ""}
        />
      </WrapSelect>
    </Popover>
  );
});
export default ListedInput;
