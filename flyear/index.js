import React, { useEffect, useState, forwardRef } from "react";
import Popover from "react-popover";
import FLInput from "../flinput";
import { isServer } from "../platform.js";
import styles from "../style.module.css";
import WrapSelect from "../common/wrapselect.js";

const FlyearInput = forwardRef((props, ref) => {
  const [listData, setListData] = useState([]);
  const [openList, setOpenList] = useState(false);
  const [value, setValue] = useState(props.value);
  const [readonly, setReadonly] = useState(false);

  if (isServer) return null;

  useEffect(() => {
    if (props.value) {
      setValue(props.value);
    }
  }, [props.value]);

  const generateArrayOfYears = () => {
    var max = new Date().getFullYear();
    var min = max - 15;
    var years = [];
    for (var i = max; i >= min; i--) {
      years.push(i.toString());
    }
    return years;
  };

  useEffect(() => {
    let arrayYear = generateArrayOfYears();
    setListData(arrayYear);
  }, []);

  useEffect(() => {
    if (props.component_obj) {
      props.component_obj.onDataset = (data) => {
        setListData(data);
      };
      props.component_obj.onValueset = (value) => {
        if (value) {
          setValue(value);
        }
      };
    }
  }, []);

  //   useEffect(() => {
  //     let result = listData.filter((obj) => {
  //       return obj.value === props.value;
  //     });

  //     if (result[0]) {
  //       setValue(result[0]);
  //     } else {
  //       setValue({ label: "", value: "" }); // truong hop value truyen vao k co trong dlist data thi set mac dinh
  //     }
  //   }, [props.value, listData]);

  const popoverProps = {
    isOpen: openList,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenList(false),
    body: [
      <div>
        <div className="popsheader">{props.label}</div>
        <div className={`list-item ${styles.custom_list_year}`}>
          {listData.map((item, index) => {
            return (
              <div
                key={index}
                className={item == value ? "sx-item active" : "sx-item"}
                onClick={(e) => {
                  if (props.changeEvent) {
                    props.changeEvent(item, index);
                  }
                  setValue(item);
                  setOpenList(false);
                }}
              >
                {item}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };
  // const blurInput = () => {
  //   setReadonly(false);
  // };
  // const focusInput = () => {
  //   setOpenList(true);
  //   setReadonly(true);
  // };


  const handleClick = () => {
    setOpenList(true);
  };
  return (
    <Popover {...popoverProps}>
      <WrapSelect handleClick={handleClick}>
        <FLInput
          disable={props.disable}
          position={props.position}
          loading={props.loading}
          label={props.label}
          hideborder={props.hideborder}
          // onFocus={(e) => focusInput()}
          value={value}
          // changeEvent={changeEvent}
          required={props.required}
          dropdown={true}
          pattern={props.pattern || null}
          // onBlur={() => blurInput()}
          extend_class={props.extend_class || ""}
        />
      </WrapSelect>
    </Popover>
  );
});
export default FlyearInput;
