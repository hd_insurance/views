import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";

const DivRender = forwardRef((props, ref) => {
  const handleOnclick = () => {
    if (props.onDivClick) {
      props.onDivClick();
    }
  };
  return (
    <div className={`${props.extend_class}`} onClick={handleOnclick}>
      {props.children}
    </div>
  );
});

export default DivRender;
